﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestForm
{
    public partial class FormBattery : Form
    {
        public FormBattery()
        {
            InitializeComponent();
            timer2.Interval = 500;
            timer2.Start();
        }
        private void timer2_Tick_1(object sender, EventArgs e)
        {
            if (battery2.Value == 100)
                battery2.Value = 1;
            battery2.Value++;
        }
    }
}
