﻿namespace TestForm
{
    partial class FormBattery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.pole1 = new IndustryControls.Pole();
            this.fan3 = new IndustryControls.Fan();
            this.battery2 = new IndustryControls.Battery();
            this.SuspendLayout();
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick_1);
            // 
            // pole1
            // 
            this.pole1.Location = new System.Drawing.Point(41, 271);
            this.pole1.Name = "pole1";
            this.pole1.RotateSpeed = 1F;
            this.pole1.Size = new System.Drawing.Size(181, 181);
            this.pole1.TabIndex = 3;
            // 
            // fan3
            // 
            this.fan3.FanColor = System.Drawing.Color.LightBlue;
            this.fan3.Location = new System.Drawing.Point(282, 264);
            this.fan3.Name = "fan3";
            this.fan3.Radius = 15F;
            this.fan3.RotateSpeed = 1F;
            this.fan3.Size = new System.Drawing.Size(202, 202);
            this.fan3.TabIndex = 2;
            // 
            // battery2
            // 
            this.battery2.AlarmColor = System.Drawing.Color.Red;
            this.battery2.AlarmValue = 10F;
            this.battery2.BackColor = System.Drawing.SystemColors.Control;
            this.battery2.BatteryBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.battery2.Location = new System.Drawing.Point(178, 89);
            this.battery2.Name = "battery2";
            this.battery2.NormalColor = System.Drawing.Color.LimeGreen;
            this.battery2.Simple2DCornerRadius = 3F;
            this.battery2.Size = new System.Drawing.Size(248, 91);
            this.battery2.TabIndex = 0;
            this.battery2.Value = 1F;
            this.battery2.ValueMax = 100F;
            this.battery2.ValueMin = 0F;
            this.battery2.WarningColor = System.Drawing.Color.Orange;
            this.battery2.WarningValue = 20F;
            // 
            // FormBattery
            // 
            this.ClientSize = new System.Drawing.Size(599, 478);
            this.Controls.Add(this.pole1);
            this.Controls.Add(this.fan3);
            this.Controls.Add(this.battery2);
            this.Name = "FormBattery";
            this.ResumeLayout(false);

        }

        #endregion

        private IndustryControls.Battery battery2;
        private IndustryControls.Fan fan3;
        private System.Windows.Forms.Timer timer2;
        private IndustryControls.Pole pole1;
    }
}