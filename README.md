# IndustryControls

### 介绍
c#工业常用的控件库，持续封装中，欢迎star与fork

### 软件架构
基于.netframework 3.0进行的封装

### 类库控件
![2D电池](/IndustryControls/img/battery2D.gif)

![太极图](/IndustryControls/img/pole.gif)

![风扇](/IndustryControls/img/fan.gif)