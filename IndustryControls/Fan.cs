﻿using IndustryControls.Utils;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace IndustryControls
{
    public partial class Fan : UserControl
    {
        public Fan()
        {
            InitializeComponent();
            SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.ResizeRedraw, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            timer1.Interval = 20;
            timer1.Start();
            InitilizeColor();
        }

        #region private method

        private void InitilizeColor()
        {
            //创建渐变色
            colorBlend = new ColorBlend();
            colorBlend.Positions = new float[3] { 0f, 0.618f, 1f };
            colorBlend.Colors = new Color[3]
            {
                FeColor,
                AnColor,
                FeColor
            };
        }

        #endregion

        #region private members
        private int pieDegree = 65;
        private float radius = 15f;
        private ColorBlend colorBlend;
        private Color FeColor = Color.FromArgb(118, 119, 120);
        private Color AnColor = Color.FromArgb(233, 233, 216);
        private double radiusNumber = Math.PI / 180;

        private Pen pen = new Pen(Brushes.White, 3);

        private float rotateSpeed = 1f;
        private int angle = 0;
        private Color fanColor = Color.LightBlue;
        #endregion

        #region
        public float Radius
        {
            get => radius;
            set
            {
                if (value != radius)
                {
                    radius = value;
                    Invalidate();
                }
            }
        }
        [Browsable(true)]
        [Description("设置或获取风扇的旋转速度")]
        [Category("IndustryControls")]
        public float RotateSpeed
        {
            get => rotateSpeed;
            set => rotateSpeed = value;
        }
        [Browsable(true)]
        [Description("设置或获取风扇扇叶的填充色")]
        [Category("IndustryControls")]
        public Color FanColor
        {
            get => fanColor;
            set
            {
                fanColor = value;
                Invalidate();
            }
        }
        #endregion

        protected override void OnPaint(PaintEventArgs e)
        {
            var g = e.Graphics;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            using (LinearGradientBrush brush = new LinearGradientBrush(new PointF(0, 0), new PointF(Width, 0), Color.FromArgb(150, 150, 150), Color.FromArgb(150, 150, 150)))
            {
                brush.InterpolationColors = colorBlend;
                //最外部的圆角矩形
                using (GraphicsPath path = new GraphicsPath())
                {
                    g.FillPath(brush, DrawHelper.GetRectRadiusPath(path, 0.1f * Width, 0, 0, Width, Height));
                }
            }
            //填充内部第一圆
            using (Brush brush = new SolidBrush(Color.DarkGray))
            {
                g.FillEllipse(brush, new RectangleF(0.05f * Width, 0.05f * Width, 0.9f * Width, 0.9f * Width));
                //填充内部第二个圆
                using (Brush brush2 = new SolidBrush(Color.Black))
                {
                    g.FillEllipse(brush2, new RectangleF(0.08f * Width, 0.08f * Width, Width * 0.84f, Width * 0.84f));
                }
                using (Brush brush3 = new SolidBrush(Color.White))
                {
                    g.FillEllipse(brush3, new RectangleF(0.42f * Width, 0.42f * Width, 0.16f * Width, 0.16f * Width));
                }
                g.FillEllipse(brush, new RectangleF(0.44f * Width, 0.44f * Width, 0.12f * Width, 0.12f * Width));

            }
            g.TranslateTransform(Width / 2, Width / 2);
            g.RotateTransform(angle);
            //using (LinearGradientBrush brush = new LinearGradientBrush(new PointF(0, 0), new PointF(Width, 0), Color.FromArgb(150, 150, 150), Color.FromArgb(150, 150, 150)))
            using (Brush brush = new SolidBrush(fanColor))
            {
                //brush.InterpolationColors = colorBlend;
                //绘制第一片扇叶
                using (GraphicsPath path = new GraphicsPath())
                {
                    path.AddArc(new RectangleF(0.15f * Width - 0.5f * Width, 0.15f * Width - 0.5f * Width, 0.7f * Width, 0.7f * Width), 0, pieDegree);
                    path.AddCurve(new PointF[]
                    {
                    new PointF(0.58f * Width-0.5f*Width, 0.5f * Width-0.5f*Width),
                    new PointF(0.70f * Width-0.5f*Width, 0.46f * Width-0.5f*Width),
                    new PointF(0.78f * Width-0.5f*Width, 0.45f * Width-0.5f*Width),
                    new PointF(0.82f * Width-0.5f*Width, 0.46f * Width-0.5f*Width),
                    new PointF(0.84f * Width-0.5f*Width, 0.48f * Width-0.5f*Width),
                    new PointF(0.85f * Width-0.5f*Width, 0.5f * Width-0.5f*Width)
                    });
                    path.AddCurve(new PointF[]
                    {
                 new PointF(0.5f*Width+(float)Math.Cos(pieDegree*radiusNumber)*0.08f*Width-0.5f*Width,0.5f*Width+(float)Math.Sin(pieDegree*radiusNumber)*0.08f*Width-0.5f*Width),
                 new PointF(0.5f*Width+(float)Math.Cos(pieDegree*radiusNumber)*0.35f*Width-0.05F*Width-0.5f*Width,0.5f*Width+(float)Math.Sin(pieDegree*radiusNumber)*0.35f*Width-0.15F*Width-0.5f*Width),
                 new PointF(0.5f*Width+(float)Math.Cos(pieDegree*radiusNumber)*0.35f*Width-0.03F*Width-0.5f*Width,0.5f*Width+(float)Math.Sin(pieDegree*radiusNumber)*0.35f*Width-0.08F*Width-0.5f*Width),
                 new PointF(0.5f*Width+(float)Math.Cos(pieDegree*radiusNumber)*0.35f*Width-0.02F*Width-0.5f*Width,0.5f*Width+(float)Math.Sin(pieDegree*radiusNumber)*0.35f*Width-0.02F*Width-0.5f*Width),
                 new PointF(0.5f*Width+(float)Math.Cos(pieDegree*radiusNumber)*0.35f*Width-0.5f*Width,0.5f*Width+(float)Math.Sin(pieDegree*radiusNumber)*0.35f*Width-0.5f*Width)
                     });
                    path.AddArc(new RectangleF(0.42f * Width - 0.5f * Width, 0.42f * Width - 0.5f * Width, 0.16f * Width, 0.16f * Width), 0, pieDegree);
                    g.FillPath(brush, path);
                }
                //绘制第二片扇叶
                using (GraphicsPath path = new GraphicsPath())
                {
                    path.AddArc(new RectangleF(0.15f * Width - 0.5f * Width, 0.15f * Width - 0.5f * Width, 0.7f * Width, 0.7f * Width), 120, pieDegree);
                    path.AddCurve(new PointF[]
                    {
                    new PointF(0.5f*Width-(float)Math.Cos(60*radiusNumber)*0.08f*Width-0.5f*Width,.5f * Width+(float)Math.Sin(60*radiusNumber)*0.08f*Width-0.5f*Width),
                    new PointF(0.5f*Width-(float)Math.Cos(60*radiusNumber)*0.35f*Width+0.12f*Width-0.5f*Width, 0.5f * Width+(float)Math.Sin(60*radiusNumber)*0.35f*Width-0.12f*Width-0.5f*Width),
                    new PointF(0.5f*Width-(float)Math.Cos(60*radiusNumber)*0.35f*Width+0.08f*Width-0.5f*Width, 0.5f * Width+(float)Math.Sin(60*radiusNumber)*0.35f*Width-0.015f*Width-0.5f*Width),
                    new PointF(0.5f*Width-(float)Math.Cos(60*radiusNumber)*0.35f*Width+0.05f*Width-0.5f*Width, 0.5f * Width+(float)Math.Sin(60*radiusNumber)*0.35f*Width+0.005f*Width-0.5f*Width),
                    new PointF(0.5f*Width-(float)Math.Cos(60*radiusNumber)*0.35f*Width-0.5f*Width, 0.5f * Width+(float)Math.Sin(60*radiusNumber)*0.35f*Width-0.5f*Width)
                    });
                    path.AddCurve(new PointF[]
                    {
                   new PointF(0.5f*Width-(float)Math.Cos(5*radiusNumber)*0.08f*Width-0.5f*Width,0.5f * Width-(float)Math.Sin(5*radiusNumber)*0.08f*Width-0.5f*Width),
                    new PointF(0.5f*Width-(float)Math.Cos(5*radiusNumber)*0.35f*Width+0.16f*Width-0.5f*Width, 0.5f * Width-(float)Math.Sin(5*radiusNumber)*0.35f*Width-0.02F*Width+0.05f*Width-0.5f*Width),
                    new PointF(0.5f*Width-(float)Math.Cos(5*radiusNumber)*0.35f*Width+0.05f*Width-0.5f*Width, 0.5f * Width-(float)Math.Sin(5*radiusNumber)*0.35f*Width-0.02F*Width+0.02f*Width-0.5f*Width),
                    new PointF(0.5f*Width-(float)Math.Cos(5*radiusNumber)*0.35f*Width+0.02f*Width-0.5f*Width, 0.5f * Width-(float)Math.Sin(5*radiusNumber)*0.35f*Width-0.01F*Width-0.5f*Width),
                    new PointF(0.5f*Width-(float)Math.Cos(5*radiusNumber)*0.35f*Width-0.5f*Width, 0.5f * Width-(float)Math.Sin(5*radiusNumber)*0.35f*Width-0.5f*Width)
                     });
                    path.AddArc(new RectangleF(0.42f * Width - 0.5f * Width, 0.42f * Width - 0.5f * Width, 0.16f * Width, 0.16f * Width), 120, pieDegree);
                    g.FillPath(brush, path);
                }
                //绘制第三片扇叶
                using (GraphicsPath path = new GraphicsPath())
                {
                    path.AddArc(new RectangleF(0.15f * Width - 0.5f * Width, 0.15f * Width - 0.5f * Width, 0.7f * Width, 0.7f * Width), 240, pieDegree);
                    path.AddCurve(new PointF[] {
                        new PointF(0.5f*Width-(float)Math.Cos(60*radiusNumber)*0.08f*Width-0.5f*Width,0.5f*Width-(float)Math.Sin(60*radiusNumber)*0.08f*Width-0.5f*Width),
                        new PointF(0.5f*Width-(float)Math.Cos(60*radiusNumber)*0.35f*Width+0.05f*Width-0.5f*Width,0.5f*Width-(float)Math.Sin(60*radiusNumber)*0.35f*Width+0.16f*Width-0.5f*Width),
                        new PointF(0.5f*Width-(float)Math.Cos(60*radiusNumber)*0.35f*Width-0.01f*Width-0.5f*Width,0.5f*Width-(float)Math.Sin(60*radiusNumber)*0.35f*Width+0.06f*Width-0.5f*Width),
                        new PointF(0.5f*Width-(float)Math.Cos(60*radiusNumber)*0.35f*Width-0.5f*Width,0.5f*Width-(float)Math.Sin(60*radiusNumber)*0.35f*Width-0.5f*Width)
                    });
                    path.AddCurve(new PointF[] {
                        new PointF(0.5f*Width+(float)Math.Cos(55*radiusNumber)*0.08f*Width-0.5f*Width,0.5f*Width-(float)Math.Sin(55*radiusNumber)*0.08f*Width-0.5f*Width),
                        new PointF(0.5f*Width+(float)Math.Cos(55*radiusNumber)*0.35f*Width-0.12f*Width-0.5f*Width,0.5f*Width-(float)Math.Sin(55*radiusNumber)*0.35f*Width+0.15f*Width-0.5f*Width),
                        new PointF(0.5f*Width+(float)Math.Cos(55*radiusNumber)*0.35f*Width+0.005f*Width-0.5f*Width,0.5f*Width-(float)Math.Sin(55*radiusNumber)*0.35f*Width+0.045f*Width-0.5f*Width),
                        new PointF(0.5f*Width+(float)Math.Cos(55*radiusNumber)*0.35f*Width-0.5f*Width,0.5f*Width-(float)Math.Sin(55*radiusNumber)*0.35f*Width-0.5f*Width)
                    });
                    path.AddArc(new RectangleF(0.42f * Width - 0.5f * Width, 0.42f * Width - 0.5f * Width, 0.16f * Width, 0.16f * Width), 240, pieDegree);
                    g.FillPath(brush, path);
                }
            }
            g.TranslateTransform(-Width / 2, -Width / 2);
            base.OnPaint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            int size = Math.Max(Width, Height);
            Width = Height = size;
            base.OnResize(e);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (RotateSpeed != 0)
            {
                angle += (int)(RotateSpeed * 360f / Math.PI / 20);
                if (angle >= 360)
                    angle -= 360;
                if (angle <= -360)
                    angle += 360;
            }
            Invalidate();
        }
    }
}
