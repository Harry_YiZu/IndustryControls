﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Text;

namespace IndustryControls
{
    /*非工业控件，娱乐绘制*/
    public partial class Pole : UserControl
    {
        public Pole()
        {
            InitializeComponent();
            SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, true);//用户自绘控件
            SetStyle(ControlStyles.ResizeRedraw, true);//控件尺寸更改时，触发重绘
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);//双缓冲绘图
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            timer1.Interval = 20;
            timer1.Start();
        }

        #region private members
        private float rotateSpeed = 1f;
        private int angle = 0;
        #endregion

        #region public members
        public float RotateSpeed { get => rotateSpeed; set => rotateSpeed = value; }
        #endregion

        #region override onpaint
        /// <summary>
        /// Resize时，保证控件尺寸为矩形
        /// </summary>
        /// <param name="e"></param>
        protected override void OnResize(EventArgs e)
        {
            int size = Math.Max(Width, Height);
            Width = Height = size;
            base.OnResize(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            g.TranslateTransform(Width / 2, Width / 2);
            g.RotateTransform(angle);
            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddArc(-Width / 2, -Width / 2, Width, Width, 0, 180);
                path.AddArc(-Width / 2, -Width / 4, Width / 2, Width / 2, 0, -180);
                path.AddArc(0, -Width / 4, Width / 2, Width / 2, 0, 180);
                g.FillPath(Brushes.Black, path);
                g.FillPie(Brushes.White, new Rectangle(Width * 90 / 400 - Width / 2, Width * 190 / 400 - Width / 2, Width * 50 / 400, Width * 50 / 400), 0, 360);
            }
            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddArc(-Width / 2, -Width / 2, Width, Width, 0, -180);
                path.AddArc(-Width / 2, -Width / 4, Width / 2, Width / 2, 0, -180);
                path.AddArc(0, -Width / 4, Width / 2, Width / 2, 0, 180);
                g.FillPath(Brushes.White, path);
                g.FillPie(Brushes.Black, new Rectangle(Width * 290 / 400 - Width / 2, Width * 190 / 400 - Width / 2, Width * 50 / 400, Width * 50 / 400), 0, 360);
            }
            g.TranslateTransform(-Width / 2, -Width / 2);
            base.OnPaint(e);
        }
        #endregion

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (rotateSpeed != 0)
            {
                angle += (int)(rotateSpeed * 360f / Math.PI / 20);
                if (angle >= 360)
                    angle -= 360;
                if (angle <= -360)
                    angle += 360;
            }
            Invalidate();
        }
    }
}
