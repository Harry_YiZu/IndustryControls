﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;

namespace IndustryControls.Utils
{
    public static class DrawHelper
    {
        public static void DrawRectangleWithRadius(Graphics g, float radius, Pen pen, float x, float y, float width, float height)
        {
            //上边
            g.DrawLine(pen, x + radius, y, x + width - radius - 0f, y);
            //下边
            g.DrawLine(pen, x + radius, y + height - 0f, x + width - radius - 0f, y + height - 0f);
            //左边
            g.DrawLine(pen, x, y + radius, x, y + height - radius - 0f);
            //右边
            g.DrawLine(pen, x + width - 0f, y + radius, x + width - 0f, y + height - radius - 0f);
            if (radius > 0)
            {
                //左上角
                g.DrawArc(pen, x, y, radius * 2, radius * 2, 180, 90);//180, 90
                                                                      //右上角
                g.DrawArc(pen, x + width - radius * 2 - 0f, y, radius * 2, radius * 2, 270, 90);//270, 90
                                                                                                //左下角
                g.DrawArc(pen, x, y + height - radius * 2 - 0f, radius * 2, radius * 2, 90, 90);//90, 90
                                                                                                //右下角
                g.DrawArc(pen, x + width - radius * 2 - 0f, y + height - radius * 2 - 0f, radius * 2, radius * 2, 0, 90);//0, 90
            }
        }

        public static GraphicsPath GetRectRadiusPath(GraphicsPath p, float radius, float x, float y, float width, float height)
        {
            if (radius <= 0)
            {
                radius = 0;
                //上边
                p.AddLine(x + radius, y, x + width - radius - 0f, y);
                //右边
                p.AddLine(x + width - 0f, y + radius, x + width - 0f, y + height - radius - 0f);
                //下边
                p.AddLine(x + radius, y + height - 0f, x + width - radius - 0f, y + height - 0f);
                //左边
                p.AddLine(x, y + radius, x, y + height - radius - 0f);
            }
            //上边
            p.AddLine(x + radius, y, x + width - radius - 0f, y);
            if (radius > 0)
                //右上
                p.AddArc(x + width - radius * 2 - 0f, y, radius * 2, radius * 2, 270, 90);
            //右边
            p.AddLine(x + width - 0f, y + radius, x + width - 0f, y + height - radius - 0f);
            if (radius > 0)
                //右下
                p.AddArc(x + width - radius * 2 - 0f, y + height - radius * 2 - 0f, radius * 2, radius * 2, 0, 90);
            //下边
            p.AddLine(x + radius, y + height - 0f, x + width - radius - 0f, y + height - 0f);
            if (radius > 0)
                //左下
                p.AddArc(x, y + height - radius * 2 - 0f, radius * 2, radius * 2, 90, 90);
            //左边
            p.AddLine(x, y + radius, x, y + height - radius - 0f);
            if (radius > 0)
                //左上
                p.AddArc(x, y, radius * 2, radius * 2, 180, 90);

            return p;

        }
    }
}
