﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IndustryControls.Utils
{
    /// <summary>
    /// 控件的放置方式
    /// </summary>
   public enum ControlOrientation
    {
        /// <summary>
        /// 控件水平放置
        /// </summary>
        Horizontal,
        /// <summary>
        /// 控件垂直放置
        /// </summary>
        Vertical,
    }
}
