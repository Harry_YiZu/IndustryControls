﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using IndustryControls.Utils;
using System.Drawing.Drawing2D;
using System.Threading;

namespace IndustryControls
{
    public partial class Battery : UserControl
    {
        public Battery()
        {
            InitializeComponent();
            SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, true);//用户自绘控件
            SetStyle(ControlStyles.ResizeRedraw, true);//控件尺寸更改时，触发重绘
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);//双缓冲绘图
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            animation.AnimationType = AnimationType.CircEaseInOut;
            animation.AnimationComing += Animation_AnimationComing;
            animation.Duration = 400;
        }
        /// <summary>
        /// 缓动函数的值触发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Animation_AnimationComing(object sender, AnimationEventArgs e)
        {
            easeFunctionValue = e.NowValue;
            Invalidate();
        }

        #region private member
        private float easeFunctionValue;
        /// <summary>
        /// 电池的背景色
        /// </summary>
        private Color batteryBackColor = Color.FromArgb(0, 0, 0);
        /// <summary>
        /// 电池的背景画刷
        /// </summary>
        private Brush batteryBackBrush = new SolidBrush(Color.FromArgb(0, 0, 0));
        /// <summary>
        /// 电池的背景画笔
        /// </summary>
        private Pen batteryBackPen = new Pen(Color.FromArgb(0, 0, 0), 2);
        /// <summary>
        /// 电池的放置方向
        /// </summary>
        private ControlOrientation _batteryOrientation = ControlOrientation.Horizontal;
        /// <summary>
        /// 电池的绘制类型
        /// </summary>
        private BatteryMode _batteryMode = BatteryMode.Simple2D;

        private SimpleLock simpleLock = new SimpleLock();

        private delegate void ValueChangeDelegate();

        private float value_max = 100f;
        private float value_min = 0f;
        private float value_now = 1f;
        private float value_pre = 1f;

        private float simple2DCornerRadius = 3f;

        private Color warningColor = Color.Orange;
        private Color alarmColor = Color.Red;
        private Color normalColor = Color.DarkGray;

        private float warningValue = 20;
        private float alarmValue = 10;

        private Animation animation = new Animation();

        #endregion

        #region public member
        [Browsable(true)]
        [Description("设置或获取电池的最大值")]
        [Category("IndustryControls")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public float ValueMax
        {
            get
            {
                return value_max;
            }
            set
            {
                value_max = value;
                Invalidate();
            }
        }
        [Browsable(true)]
        [Description("设置或获取电池的最小值")]
        [Category("IndustryControls")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public float ValueMin
        {
            get
            {
                return value_min;
            }
            set
            {
                value_min = value;
                Invalidate();
            }
        }
        [Browsable(true)]
        [Description("设置或获取电池的当前值")]
        [Category("IndustryControls")]
        [DefaultValue(typeof(float), "1f")]
        public float Value
        {
            get
            {
                return value_now;
            }
            set
            {
                if (value != value_now)
                {
                    value_pre = value_now;
                    value_now = value;
                    animation.AnimationStart();
                }
            }
        }
        [Browsable(true)]
        [Description("设置或获取控件背景颜色")]
        [Category("IndustryControls")]
        [DefaultValue(typeof(Color), "Transparent")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public override Color BackColor
        {
            get => base.BackColor;
            set => base.BackColor = value;
        }
        [Browsable(true)]
        [Description("设置或获取电池的背景颜色")]
        [Category("IndustryControls")]
        [DefaultValue(typeof(Color), "[248, 248, 255]")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public Color BatteryBackColor
        {
            get
            {
                return batteryBackColor;
            }
            set
            {
                batteryBackColor = value;
                batteryBackBrush.Dispose();
                batteryBackBrush = new SolidBrush(batteryBackColor);
                batteryBackPen.Dispose();
                batteryBackPen = new Pen(batteryBackColor);
                Invalidate();
            }
        }
        [Browsable(true)]
        [Description("设置或获取电池的摆放方向")]
        [Category("IndustryControls")]
        [DefaultValue(typeof(ControlOrientation), "Horizontal")]
        public ControlOrientation BatteryOrietation
        {
            get
            {
                return _batteryOrientation;
            }
            set
            {
                _batteryOrientation = value;
                Invalidate();
            }
        }
        [Browsable(true)]
        [Description("设置或获取电池的显示类别")]
        [Category("IndustryControls")]
        [DefaultValue(typeof(BatteryMode), "Simple2D")]
        public BatteryMode ShowMode
        {
            get
            {
                return _batteryMode;
            }
            set
            {
                _batteryMode = value;
                Invalidate();
            }
        }
        [Browsable(true)]
        [Description("设置或获取2D电池的矩形圆角")]
        [Category("IndustryControls")]
        [DefaultValue(typeof(float), "0")]
        public float Simple2DCornerRadius
        {
            get => simple2DCornerRadius;
            set
            {
                if (value < 0)
                    value = 0;
                simple2DCornerRadius = value;
                Invalidate();
            }
        }
        [Description("设置或获取电池的Warning填充色"), Category("IndustryControls")]
        public Color WarningColor
        {
            get => warningColor;
            set => warningColor = value;
        }
        [Description("设置或获取电池的Alarm填充色"), Category("IndustryControls")]
        public Color AlarmColor { get => alarmColor; set => alarmColor = value; }
        [Description("设置或获取电池的Normal填充色"), Category("IndustryControls")]
        public Color NormalColor { get => normalColor; set => normalColor = value; }
        [Description("设置或获取电池的Warning上限值"), Category("IndustryControls")]
        [DefaultValue(typeof(Color), "Colors.DarkGray")]
        public float WarningValue { get => warningValue; set => warningValue = value; }
        [Description("设置或获取电池的Alarm上限值"), Category("IndustryControls")]
        public float AlarmValue { get => alarmValue; set => alarmValue = value; }

        #endregion

        #region override method
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            if (_batteryMode == BatteryMode.Simple2D)
            {
                DrawSimple2D(g);
            }
            else
            {
                DrawSimple3D(g);
            }

            base.OnPaint(e);
        }
        /// <summary>
        /// 2D电池
        /// </summary>
        /// <param name="g"></param>
        private void DrawSimple2D(Graphics g)
        {
            if (BatteryOrietation == ControlOrientation.Horizontal)
            {
                //外层圆角矩形
                DrawHelper.DrawRectangleWithRadius(g, simple2DCornerRadius, batteryBackPen, 0, 0, Width * 0.9f, 0.99f * Height);
                //内层圆角矩形
                using (GraphicsPath path = new GraphicsPath())
                {
                    float totalWidth = (Width * 0.9f - 0.1f * Height) / 100;
                    float paintWidth = value_pre * totalWidth + (value_now - value_pre) * totalWidth * easeFunctionValue;
                    using (Brush brush = new SolidBrush(GetFillColor(value_now)))
                    {
                        g.FillPath(brush, DrawHelper.GetRectRadiusPath(path, simple2DCornerRadius, 0.05f * Height, 0.05f * Height, paintWidth, 0.9f * Height));
                    }
                }
                //绘制电极
                using (GraphicsPath path = new GraphicsPath())
                {
                    DrawHelper.DrawRectangleWithRadius(g, 2f, batteryBackPen, Width * 0.9f + 3f, Height * 0.2f, 0.05f * Width, 0.6f * Height);
                    g.FillPath(Brushes.DarkGray, DrawHelper.GetRectRadiusPath(path, 4f, Width * 0.9f + 3f, Height * 0.2f, 0.05f * Width, 0.6f * Height));
                }
            }
            else
            {

            }
        }

        private Color GetFillColor(float value)
        {
            if (value > warningValue)
                return normalColor;
            else if (value > alarmValue && value <= warningValue)
                return warningColor;
            else
                return alarmColor;
        }

        private void DrawSimple3D(Graphics g)
        {

        }
        #endregion
    }

    public enum BatteryMode
    {
        Simple2D,
        Simple3D
    }
}
